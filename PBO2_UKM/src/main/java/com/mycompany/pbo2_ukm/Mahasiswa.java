/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_ukm;

/**
 *
 * @author asus
 */
public class  Mahasiswa extends Penduduk{
    private String nim;

    public Mahasiswa() {
    }

    public Mahasiswa(String nim, String nama, String tempat_tanggal_lahir) {
        super(nama, tempat_tanggal_lahir);
        this.nim = nim;
    }

    void setNim (String dataNim){
        this.nim =dataNim;
    }
    String getNim (){
        return nim;
    }

    @Override
    double hitungluran() {
        double iuran = Double.parseDouble(getNim());
        return iuran/10000;
    }
    
}
