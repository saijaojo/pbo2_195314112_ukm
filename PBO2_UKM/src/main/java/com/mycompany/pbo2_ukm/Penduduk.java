/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_ukm;

/**
 *
 * @author asus
 */
public abstract class Penduduk {
    private String nama;
    private String tempat_tanggal_lahir;

    public Penduduk() {
    }

    public Penduduk(String nama, String tempat_tanggal_lahir) {
        this.nama = nama;
        this.tempat_tanggal_lahir = tempat_tanggal_lahir;
    }
    
    void setNama (String dataNama){
        this.nama = dataNama;
    }
    String getNama (){
        return nama;
    }
    void setTempat_tanggal_lahir (String dataTempat_tanggal_lahir){
        this.tempat_tanggal_lahir =dataTempat_tanggal_lahir;
    }
    String getgetTempat_tanggal_lahir (){
        return tempat_tanggal_lahir;
    }
    abstract double hitungluran ();
}
